**REPORTE GERENCIAL DE DATOS DE FIFA19**

**Autor:  Edison Sanango**

**Objetivo:**  Generar un reporte gerencial que simule la extracción y limpieza de datos, y su posterior procesamiento para obtener información a través de la aplicación de los conocimientos vistos en el presente curso desarrollado en Python mediante Jupyter Notebook del ambiente Anaconda. 

**Descripción sobre el archivo:**  El  archivo original pesaba 9.16 Mb con 18206 filas y 89 columnas, después del análisis y limpieza de los mismos se redujo a 1.66 Mb con 17918 filas con 19 columnas. 

**Descripción sobre los gráficos:**  El diseño de los 21 gráficos se basaron en los 4 principios de Cairo: **Veracidad**,  este principio indica que no se debe presentar gráficos engañosos. **Belleza**,  este principio clama por el uso de fuentes y colores que sean estéticamente agradables. Tercero,  **Funcionalidad**,  significa que se debe elegir bien el tipo de gráfico para mostrar los datos.  Finalmente,  **Perspicacia**,  o una mezcla de revelación y esclarecimiento, busca sorprender al receptor y causar una impresión nueva al ver los gráficos. 

Se puede encontrar información sobre estos principios en el libro The Truthful Art de Alberto Cairo.
**Aclaración:** Este archivo fue extraído del repositorio público Kaggle con propósitos educativos. Por ende, los créditos se los atorga a la persona o entidad que lo haya proporcionado.

**1. Los 10 países que tienen más de 99 jugadores con mayor overall promedio.**

Portugal es el país con mayor overall promedio con 71.327~. Esto representa una pequeña diferencia con Brasil que obtuvo 71.274~. Solo se presentaron los 10 países con mejor resultado y se excluyeron a todos los países que en sus filas no tengan un mínimo de 99 jugadores. Esto se debe a que en caso de tener pocos jugadores este valor sufre una distorsión.
<img src="Fifa19/Plots/p1.png" style="height: 50px; width:50px;"/>

**2. Los 10 países con mayor valor de mercado acumulado de sus jugadores en dólares.**

España es el país con mayor valor acumulado de sus jugadores con 4.706.430.000 esto supone una diferencia de 900.000.000 con el segundo país que es Brasil. 
<img src="Fifa19/Plots/p2.png" style="height: 50px; width:50px;"/>

**3. Los 10 clubes con mayor overall promedio.**

Juventus se posiciona como el mejor equipo dentro de Fifa 19 superando por 1 punto a FC Bayern München. Sorprende la posición del Real Madrid, es el octavo mejor. 
<img src="Fifa19/Plots/p3.png" style="height: 50px; width:50px;"/>

**4. Los 10 clubes con mayor valor de mercado acumulado.**

A pesar de que Real Madrid no es el equipo con mejor overall; sí es el equipo con los jugadores que más dinero acumulado tiene. Tiene una diferencia de 22.000.000 con FC Barcelona su inmediato perseguidor. 
<img src="Fifa19/Plots/p4.png" style="height: 50px; width:50px;"/>


**5. Los 10 dorsales de camiseta con mayor overall promedio.**

Aunque no es un número tradicional, el número 76 es el dorsal con mayor averall promedio, 72.5. Para futuros análisis se debe plantear una cantidad mínima de repeticiones del dorsal ya que este número puede ser generado por un sólo jugador. 
<img src="Fifa19/Plots/p5.png" style="height: 50px; width:50px;"/>

**6. Los 10 jugadores con más tiempo en su club en años.**

O. Pérez es el jugador que más ha esetado en el mismo club, 29. Existe una diferencia de 7 años a sus mayores perseguidores.  
<img src="Fifa19/Plots/p6.png" style="height: 50px; width:50px;"/>

**7. Las 10 nacionalidades que tienen más jugadores.**

Inglaterra es el claro dominador con 1657. Sin embargo, un punto a destacar es la cantidad de jugadores de Colombia que están a poca cantidad de Italia, un país tradicional en el mundo futbolístico. 
<img src="Fifa19/Plots/p7.png" style="height: 50px; width:50px;"/>

**8. Las 10 nacionalidades que tienen menos jugadores pero con más de 22 jugadores**

Para armar un equipo regular, mínimo se necesitan 22 jugadores. Por tal razón se eliminaron los países con menor cantidad que 22. Ante esto, se determina que la selección con menos jugadores es Hungría. Cabe mencionar que Ecuador se encuentra en esta estadística, lo cual a comparación de Colombia deja en evidencia la enorme diferencia de las realidades entre los dos países.  
<img src="Fifa19/Plots/p8.png" style="height: 50px; width:50px;"/>

**9. Pie preferido por los jugadores.**

El pie predominante es el derecho. Aunque no es algo que se desarrolla como habilidad, si es curioso la gran diferencia hay una relacipon de 3 a 1 aproximadamente, lo cual es desproporcionado.  
<img src="Fifa19/Plots/p9.png" style="height: 50px; width:50px;"/>

**10. Los 10 jugadores con mayor salario.**

L. Messi percibe el mayor salario mensual, 565.000. Este tiene una ventaja considerable de 110.000 con el segundo mejor pagado que es L. Suárez.   
<img src="Fifa19/Plots/p10.png" style="height: 50px; width:50px;"/>

**11. Las 10 posiciones más repetidas.**

La posición de Delantero (ST) es la más común seguido de los centrales (CB) y de los arqueros (GK) tres posiciones indispensables en todos los equipos.  
<img src="Fifa19/Plots/p11.png" style="height: 50px; width:50px;"/>

**12. Los 10 equipos que tiene más jugadores prestados a otros equipos.**

Uno de los resultados más sorprendentes es que Atalanta sea el equipo que presta más jugadores. A pesar de no ser un equipo gigante de Italia, encabeza esta estadística, al igual que Sassuolo. Pequeños equipos que manejan bien sus recursos. 
<img src="Fifa19/Plots/p12.png" style="height: 50px; width:50px;"/>

**13. Los 10 jugadores más altos.**

Este realmente fue un empate, entre D, Hodzic y T. Holý ambos con 205.74 cm. Se tiene la misma estatura ya que, todos los datos se transformaron a centímentros.
<img src="Fifa19/Plots/p13.png" style="height: 50px; width:50px;"/>

**14. Los 10 jugadores más bajos.**

Este realmente fue un empate, entre tres jugadores K. Yamaguchi, H. Nakagawa, N Barrios. Los tres con 154.94 cm al igual que la pregunta 13, esto se debe a la transformación de pulgadas a centímetros. 
<img src="Fifa19/Plots/p14.png" style="height: 50px; width:50px;"/>

**15. Los 10 jugadores más pesados.**

Con una gran diferencia A. Akinfenwa es el jugador más pesado 243 lbs equivalente a 110 kg aproximadamente. 
<img src="Fifa19/Plots/p15.png" style="height: 50px; width:50px;"/>

**16. Los 10 jugadores más livianos.**

Existe un empate entre B. Al Mutairi y K. Yamaguchi. Son los jugadores más livianos con 110 lbs. Curiosamente, K. Yamaguchi también estuvo entre los jugadores más bajos. Esto indica que en general, es el jugador con menor complexión física en el fútbol profesional. 
<img src="Fifa19/Plots/p16.png" style="height: 50px; width:50px;"/>

**17. Los 10 jugadores que tienen mayor aceleración.**

Sorprendentemente, Adama empata con Douglas Costa. Igualmente, los mejores jugadores del mundo no entran en este top.  
<img src="Fifa19/Plots/p17.png" style="height: 50px; width:50px;"/>

**18. Los 10 jugadores con la cláusula de recisión más alta.**

Neymar Jr. posee la cantidad más alta de dinero en caso de que algún equipo quiera contratar sus servicios antes del término de su contrato. Actualmente, ronda los 230.000.000. Una cantidad inimaginable.  
<img src="Fifa19/Plots/p18.png" style="height: 50px; width:50px;"/>

**19. Los 10 jugadores más viejos.**

Esta pregunta está muy relacionada con la 6. Ya que repite ganador. O. Pérez es el jugador más viejo con 45 años. 
<img src="Fifa19/Plots/p19.png" style="height: 50px; width:50px;"/>

**20. Los 10 dorsales más repetidos.**

Hay una tendencia a tener más jugadores de ataque de defensa. Esto se evidencia en esta pregunta. El 8 es el dorsal más repetido, con 602 veces. Igualmente, los 4 primeros lugares son, tradicionalmente, ofensivos: 8, 7, 10, 11. Esto deja en claro que existen más jugadores de ofensiva que de defensiva.  
<img src="Fifa19/Plots/p20.png" style="height: 50px; width:50px;"/>

**21. Las 10 nacionalidades con mayor salario acumulado.**

Finalmente, España es el país que posee mayor salario mensual acumulado entre sus jugadores. Con una cantidad de 17.500.000 aproximadamente, España es el país que más dinero obtiene en salario de sus jugadores.
<img src="Fifa19/Plots/p21.png" style="height: 50px; width:50px;"/>
