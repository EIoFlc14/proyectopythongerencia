1. Los 10 países que tienen más de 99 jugadores con mayor overall promedio.
2. Los 10 países con mayor valor de mercado acumulado.
3. Los 10 clubes con mayor overall promedio.
4. Los 10 clubes con mayor valor de mercado acumulado.
5. Los 10 dorsales de camiseta con mayor overall promedio.
6. Los 10 jugadores con más tiempo en su club.
7. Las 10 nacionalidades que tienen más jugadores.
8. Las 10 nacionalidades que tienen menos jugadores.
9. Pie preferido por los jugadores.
10. Los 10 jugadores con mayor salario.
11. Posiciones más repetidas (Ejm: RF, ST, LW de la columna Position, cuando vea cuantas son, indicar ese número en la pregunta)
12. Los # equipos que tiene más jugadores prestados a otros equipos. (poner el valor de la pregunta)
13. Los 10 jugadores más altos.
14. Los 10 jugadores más bajos.
15. Los 10 jugadores más pesados.
16. Los 10 jugadores más livianos.
17. Los 10 jugadores que tienen mayor aceleración
18. Los 10 jugadores con la cláusula de recisión más alta.
19. Los 10 jugadores más viejos 
20. Los 10 jugadores más jóvenes
21. Los 10 dorsales más repetidos.
22. Las 10 nacionalidades con mayor salario acumulado.

