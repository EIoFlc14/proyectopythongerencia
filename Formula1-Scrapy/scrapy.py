import requests
from bs4 import BeautifulSoup as bfsp
import numpy as np
import pandas as pd
import datetime 

def scrapy_dataset1_2():
    years = np.arange(1950,2020)
    
    # dataset 1 
    teams = []
    points = []
    drivers = []

    # dataset 2
    date = []
    event = []
    circuit = []
    winning_driver = []
    winning_constructor = []
    laps = []
    time = []

    for year in years:
        path = 'https://www.racing-statistics.com/en/seasons/'+ str(year)
        page = requests.get(path)
        soup = bfsp(page.content, 'html.parser')
        rows = soup.find_all('tr',{'itemprop':'itemListElement'})
        for row in rows:
            row = row.find_all('td')
            drivers.append(row[2].text)
            teams.append(row[4].text)
            points.append(round(float(row[5].text),2))


        table2 = soup.find('div',{'class':'blocks blocks'})
        table2 = table2.find_all('tr')
        rows = table2[1:]
        for row in rows:
            row = row.find_all('td')
            date.append(row[1].text)
            event.append(row[2].text)
            circuit.append(row[3].text)
            winning_driver.append(row[5].text)
            winning_constructor.append(row[6].text)
            laps.append(int(row[7].text))
            if row[8].text.strip(' ') != "":
                h = 0
                aux = row[8].text.split(':')
                try:
                    h = aux[0]
                    m = aux[1]
                    s = aux[2]
                except IndexError:
                    m = aux[0]
                    s = aux[1]

                s,ms = s.split(".")
                d = datetime.timedelta(hours=int(h), minutes=int(m), seconds=int(s), milliseconds=int(ms))
                time.append(d)
            else:
                d = datetime.timedelta(hours=int(0), minutes=int(0), seconds=int(0), milliseconds=int(0))
                time.append(d)
        print(year)

    # dataset 1
    drivers = pd.Series(drivers)
    points = pd.Series(points)
    teams = pd.Series(teams)
    data1 = {'drivers':drivers,'teams':teams,'points':points}
    data1 = pd.DataFrame(data1)
    data1.to_csv('data/dataset_1.csv',index=False)

    # dataset 2
    date = pd.Series(date)
    event = pd.Series(event)
    circuit = pd.Series(circuit)
    winning_driver = pd.Series(winning_driver)
    winning_constructor = pd.Series(winning_constructor)
    laps = pd.Series(laps)
    time = pd.Series(time)
    data2 = {'date':date, 'event':event, 'circuit':circuit, 'winning driver':winning_driver, 'winning constructor':winning_constructor, 'laps':laps, 'time':time}
    data2 = pd.DataFrame(data2)
    data2.to_csv('data/dataset_2.csv',index=False)


def scrapy_dataset3(): 
    path = 'https://www.racing-statistics.com/en/constructors'
    page = requests.get(path)
    soup = bfsp(page.content, 'html.parser')
    boxes = soup.find_all('div',{'class':'letterbox'})
    teams = []
    teams_scrapy = []
    for box in boxes:
        lista = box.find_all('td')
        for element in lista:
            teams.append(element.text)
            teams_scrapy.append(element.text.replace(' ','-').replace('&','amp'))


    nationalities = []
    wins = []
    poles = []
    seasons = []
    events = []
    drivers_champs = []
    constructor_champs = []
    points = []

    for team in teams_scrapy:
        path = 'https://www.racing-statistics.com/en/constructors/{}'.format(team)
        page = requests.get(path)
        soup = bfsp(page.content,'html.parser')
        nationality = soup.find('table',{'class':'bottommargin'})
        nationality = nationality.find('a').text
        if nationality.strip(' ') == "":
            nationality = 'NO'
        
        nationalities.append(nationality)
        
        fieldset = soup.find_all('fieldset',{'class':'block'})
        fieldset = fieldset[1]
        data = fieldset.find_all('td')
        if data[0].text == 'wins:':
            
            wins.append(int(data[1].text.split(' ')[0]))
            poles.append(int(data[3].text.split(' ')[0]))
            seasons.append(int(data[7].text))  # arreglar un error aquí list index out of range.
            events.append(int(data[9].text))
            drivers_champs.append(int(data[11].text.split(' ')[0]))
            constructor_champs.append(int(data[13].text.split(' ')[0]))
            points.append(float(data[15].text))
        else:
            wins.append('NO')
            poles.append('NO')
            seasons.append('NO')
            events.append('NO')
            drivers_champs.append('NO')
            constructor_champs.append('NO')
            points.append('NO')
    
        print(team)
    
    # dataset 3
    teams = pd.Series(teams)
    nationalities = pd.Series(nationalities)
    wins = pd.Series(wins)
    poles = pd.Series(poles)
    seasons = pd.Series(seasons)
    events = pd.Series(events)
    drivers_champs = pd.Series(drivers_champs)
    constructor_champs = pd.Series(constructor_champs)
    points = pd.Series(points)
    data3 = {'teams':teams, 'nationality':nationalities, 'wins':wins, 'poles':poles, 'seasons': seasons, 'events':events, 'driver championships': drivers_champs, 'constructor championships': constructor_champs, 'points': points}
    data3 = pd.DataFrame(data3)
    data3.to_csv('data/dataset_3.csv',index=False)


def scrapy_dataset4():
    path = 'https://www.racing-statistics.com/en/drivers'
    page = requests.get(path)
    soup = bfsp(page.content,'html.parser')
    boxes = soup.find_all('div',{'class':'letterbox'})
    drivers_scrapy = []
    drivers = []
    for box in boxes:
        data = box.find_all('a')
        for driver in data:
            lastname, name = driver.text.split(', ')
            drivers.append(name + ' ' + lastname)
            name = name.replace(' ','-')
            lastname = lastname.replace(' ','-')
            driver = name + '-' + lastname
            drivers_scrapy.append(driver)
            print(driver)

    print('------------------')

    nationalities = []
    podiums = []
    wins = []
    poles = []
    championships = []
    seasons = [] #
    events = [] #
    starts = [] #
    points = []
    retirements = []


    for driver in drivers_scrapy:
        print(driver)
        name = driver.replace('ü','ue').replace('ø','o').replace("'",'').replace('š','s').replace('ä','ae').replace('.','').replace('ö','oe')  
        path = 'https://www.racing-statistics.com/en/drivers/{}'.format(name)
        page = requests.get(path)
        soup = bfsp(page.content, 'html.parser')

        nationality = soup.find('table',{'class':'bottommargin'})
        nationality = nationality.find('span',{'itemprop':'nationality'}).text
        if nationality.strip(' ') == "":
            nationality = 'NO'
        
        nationalities.append(nationality)

        #labels = ['wins:', 'podiums:','pole positions:', 'championships:','seasons:','events:','starts:','points:','retirements:']
        boxes = soup.find_all('fieldset',{'class':'block'})
        box = boxes[1]
        data = box.find_all('td') 

        flag_wins = True
        flag_podiums = True
        flag_poles = True
        flag_championships = True
        flag_seasons = True
        flag_events = True 
        flag_starts = True 
        flag_points = True 
        flag_retirements = True 
    
        for value_datum,datum in enumerate(data):

            if datum.text == 'wins:':
                wins.append(int(data[value_datum+1].text.split(' ')[0]))
                flag_wins = False

            elif datum.text == 'podiums:':
                podiums.append(int(data[value_datum+1].text.split(' ')[0]))
                flag_podiums = False

            elif datum.text == 'pole positions:':
                poles.append(int(data[value_datum+1].text.split(' ')[0]))
                flag_poles = False

            elif datum.text == 'championships:':   
                championships.append(int(data[value_datum+1].text.split(' ')[0]))
                flag_championships = False

            elif datum.text == 'seasons:':
                seasons.append(int(data[value_datum+1].text))
                flag_seasons = False

            elif datum.text == 'events:':
                events.append(int(data[value_datum+1].text))
                flag_events = False

            elif datum.text == 'starts:':
                starts.append(int(data[value_datum+1].text))
                flag_starts = False

            elif datum.text == 'points:':
                points.append(round(float(data[value_datum+1].text.split(' ')[0]),2))
                flag_points = False

            elif datum.text == 'retirements:':
                retirements.append(int(data[value_datum+1].text.split(' ')[0]))
                flag_retirements = False

        
        if flag_wins:
             wins.append(0)
        if flag_podiums:
            podiums.append(0)
        if flag_poles:
            poles.append(0)
        if flag_championships:
            championships.append(0)
        if flag_seasons:
            seasons.append(0)
        if flag_events:
            events.append(0)
        if flag_starts:
            starts.append(0)
        if flag_points:
            points.append(0)
        if flag_retirements:
            retirements.append(0)
        

    # dataset 4
    drivers = pd.Series(drivers)
    nationalities = pd.Series(nationalities)
    wins = pd.Series(wins)
    podiums = pd.Series(podiums)
    poles = pd.Series(poles)
    championships = pd.Series(championships)
    seasons = pd.Series(seasons)
    events = pd.Series(events)
    starts = pd.Series(starts)
    points = pd.Series(points)
    retirements = pd.Series(retirements)
    data4 = {'drivers':drivers, 'nationality':nationalities, 'wins':wins, 'podiums':podiums, 'poles':poles,'championships':championships, 'seasons': seasons, 'events':events,'starts':starts, 'points': points, 'retirements':retirements}
    data4 = pd.DataFrame(data4)
    data4.to_csv('data/dataset_4.csv',index=False)






#scrapy_dataset1_2()
#scrapy_dataset3()
scrapy_dataset4()