**REPORTE GERENCIAL DE INFORMACIÓN SOBRE FÓRMULA 1 RECOPILADA CON BEAUTIFUL SOUP** 

**Autor: Edison Sanango**

**Objetivo:**  Generar un reporte gerencial que derive de la extracción mediante la librería Beautiful Soup y la posterior limpieza de datos, junto con su procesamiento para obtener información a través de la aplicación de los conocimientos vistos en el presente curso desarrollado en Python mediante Jupyter Notebook del ambiente Anaconda. 

**Descripción sobre los gráficos:**  El diseño de los 20 gráficos que serán presentados a continuación se basaron en los 4 principios de Cairo. Estos son  Veracidad,  este principio indica que no se debe presentar gráficos engañosos.  Segundo,  Belleza,  este principio clama por el uso de fuentes y colores que sean estéticamente agradables y que no saturen la imagen ni al receptor. Tercero,  Funcionalidad,  significa que se debe elegir bien el tipo de gráfico para mostrar los datos. En este caso por la naturaleza de las preguntas se optó por usar gráficos similares con pequeñas variaciones.  Finalmente,  Perspicacia,  busca sorprender al receptor y causar una impresión nueva al ver los gráficos. 

**Aclaración:**  Mediante la recolección de datos de la web. Se generaron cuatro datasets. Su página web brindó cierta dificultad para realizar web scraping por la inconsistencia de sus tablas y elementos que contienen los datos. Sin embargo, esto fue aprovechado para realizar, al mismo tiempo que el scraping, la limpieza de datos. Por tal razón, se obtuvo cuatro datasests, todos relacionados a la Fórmula 1. Cada dataset responde diferentes preguntas. Fue imposible juntarlos todos ya que, no cumpliría las reglas básicas de los datos, como columnas únicas para un atributo o datos parciales repetidos. 

De este enlace se generó el primer dataset y segundo dataset. Cabe mencionar que se manejó direcciones desde /1950 - /2019 para obtener todos los datos. El tercer dataset fue generado de este enlace que posteriormente tuvo ramificaciones al final con cada nombre de equipo ejm: .../ferrari  , .../williams. El último dataset se generó de este enlace que igualmente sufrió constantes modificaciones por el nombre del piloto, ejm: .../fernando-alonso , .../lewis-hamilton.

Los datos fueron recolectados el Viernes, 4 de septiembre del 2020. En caso de recolectar los datos nuevamente, estos cambiarán, ya que, la página web actualiza sus datos constantemente.

**1. Los 10 equipos con más puntos a lo largo de la historia de la Fórmula 1.**

Por una enorme diferencia Ferrari es el equipo con más puntos logrados en la Fórmula 1. Lo sorprendete de esta estadística es el cuarto equipo Red Bull que con solamente 15 - 16 años en la Fórmula 1 obtiene ese lugar. 
<img src="Formula1-Scrapy/Plots/p1.png" style="height: 50px; width:50px;"/>

**2. Los 10 pilotos con más puntos a lo largo de la historia de la Fórmula 1.**

El claro dominador de esta estadística por su presente es Lewis Hamilton. La mayoría de pilotos son relativamente actuales, ya que, hace pocos años el sistema de puntajes cambió drásticamente lo cual, ahora, cada carrera otorga más puntaje.   
<img src="Formula1-Scrapy/Plots/p2.png" style="height: 50px; width:50px;"/>

**3. Los 10 pilotos que más temporadas participaron.**

El primer lugar está compartido por dos pilotos ya retirados. Actualmente, quien puede tomar dicho lugar es Fernando Alonso ya que regresa por dos años más. Después, ya se verá si la estadística cambia.  
<img src="Formula1-Scrapy/Plots/p3.png" style="height: 50px; width:50px;"/>

**4. Los 10 pilotos con más puntos corriendo para un mismo equipo.**

La dupla Lewis Hamilton - Mercedes es la mejor de la historia con 2518 puntos. Este valor sigue en aumento al menos hasta finales de 2021.   
<img src="Formula1-Scrapy/Plots/p4.png" style="height: 50px; width:50px;"/>

**5. Los 10 equipos con más carreras ganadas en la F1.**

Ferrari a lo largo de aproximadamente 70 años ha logrado 239 victorias. Su único rival aunque con 61 carreras de diferencia es McLaren. Esto no cambiará en aproximadamente 5 años. Al menos no con la nueva reglamentación.  
<img src="Formula1-Scrapy/Plots/p5.png" style="height: 50px; width:50px;"/>

**6. Los 10 circuitos con más vueltas corridas acumuladas.**

El Gran Premio de Mónaco, el más deseado por los pilotos tiene en su circuito, el trazado con más vueltas acumuladas en la historia. Un total de 5383 vueltas se han corrido en este glorioso circuito. Con una larga diferencia con Monza, no parece que esta estadística vaya a cambiar al menos en 10 años. 
<img src="Formula1-Scrapy/Plots/p6.png" style="height: 50px; width:50px;"/>

**7. Los 10 pilotos con mejor porcentaje de victoria en comparación con sus carerras disputadas.**

Lee Wallard un piloto con solo 2 carreras tiene el mejor porcentaje, ya que ganó una. Por otra parte, Fangio y Ascari han logrado porcentajes altos ya que, en su tiempo fueron amplios dominadores.   
<img src="Formula1-Scrapy/Plots/p7.png" style="height: 50px; width:50px;"/>

**8. Los 10 circuitos en los que se corrieron más veces.**

El circuito que siempre ha estado en la Fórmula 1 es Monza, en los 70 años de la Fórmula 1 se han disputado 69 veces.   
<img src="Formula1-Scrapy/Plots/p8.png" style="height: 50px; width:50px;"/>

**9. Los 10 eventos que más se disputaron en la historia.**

Un evento puede disputarse en diferentes circuitos, considerando aquello, el Gran Premio Británico y el Gran Premio de Italia han sido los que siempre han estado en la fórmula 1.   
<img src="Formula1-Scrapy/Plots/p9.png" style="height: 50px; width:50px;"/>

**10. Los 10 equipos con más títulos de pilotos ganados.**

Ferrari con 14 títulos de pilotos ha sido el más ganador en este ámbito. Cabe destacar que seis títulos fueron logrados por Michael Schumacher.    
<img src="Formula1-Scrapy/Plots/p10.png" style="height: 50px; width:50px;"/>

**11. Los 10 equipos con más titulos de constructores ganados.**

En cambio, Ferrari como constructor tiene 22 títulos ganados. Este dato es diferente del anterior, un título de pilotos es logrado por los puntos únicamente de dicho piloto. En cambio, el título de constructores se logra con la suma de puntos logrado por los pilotos que compitan para dicho equipo o constructor. 
<img src="Formula1-Scrapy/Plots/p11.png" style="height: 50px; width:50px;"/>

**12. Los 10 equipos con más pole positions logradas.**

Las poles positions son las veces que un equipo largó la carrera saliendo en el primer lugar. Es natural, que Ferrari con tantos años  tenga 227 poles positions. Lo destacable son las escuderias McLaren, Williams, Mercedes que están separadas solamente por 37 poles positions. Esto en el lapso de dos años puede cambiar.  
<img src="Formula1-Scrapy/Plots/p12.png" style="height: 50px; width:50px;"/>

**13. Las 10 nacionalidades de equipos que más han participado en la F1.**

Con un amplio dominio las escuderias  Británicas dominan. Con un diferencia de 49 escuderias al siguiente país: Estados Unidos, Reino Unido ha proporcionado la mayor cantidad de escuderias del pasado y de ahora. SIendo este lugar sede de la mayor parte de escuderías actuales.  
<img src="Formula1-Scrapy/Plots/p13.png" style="height: 50px; width:50px;"/>

**14. Los 10 pilotos que más carreras ganaron.**

Michael Schumacher es el dueño de esta estadística. Sin embargo, Lewis Hamilton está a dos carreras de tomar su puesto. Esto va a suceder en el transcurso del año 2020.   
<img src="Formula1-Scrapy/Plots/p14.png" style="height: 50px; width:50px;"/>

**15. Las 10 nacionalidades  de pilotos con más eventos participados en la F1.**

La nacionalidad Británica es de la cual más pilotos han surgido y que han participado dentro de la Fórmula 1. Con una enorme ventaja sobre la nacionalidad Italiana con aproximadamente 900 eventos de diferencia.  
<img src="Formula1-Scrapy/Plots/p15.png" style="height: 50px; width:50px;"/>

**16. Los 10 pilotos con más podiums en la F1.**

Lewis Hamilton superó hace pocas carreras a Michael Shumacher, con 157 se posiciona en el primer lugar. Lo interesante de este dato es que solo Sebastian Vettel puede superarlo ya que es el único piloto activo.   
<img src="Formula1-Scrapy/Plots/p16.png" style="height: 50px; width:50px;"/>

**17. Los 10 pilotos con más retiros de carreras.**

Antonio de Cesaris un piltodo del pasado abandonó 143 carreras, es una cantidad muy elevada considerando que en promedio solo hay 18 carreras por temporada.  
<img src="Formula1-Scrapy/Plots/p17.png" style="height: 50px; width:50px;"/>

**18. Las 10 nacionalidades con más podiums acumulados.**

Los pilotos británicos han dominado la Fórmula 1 a lo largo de su historia. Con 688 podiums supera por mucha a nacionalidades como Alemana o Francesa que son los dos próximos en las estadísticas.   
<img src="Formula1-Scrapy/Plots/p18.png" style="height: 50px; width:50px;"/>

**19. Las 10  nacionalidades con más victorias ganadas.**

Un dato similar, es la nacionalidad que tiene más carreras ganadas. Igualemente, la nacionalidad Británica es la primera en esta lista con 293 victorias. Sin embargo, no tiene mucha diferencia con la nacionalidad Alemana que tiene 179 a comparación de los podiums logrados.  
<img src="Formula1-Scrapy/Plots/p19.png" style="height: 50px; width:50px;"/>

**20. Los 10 pilotos con más carreras no largadas.**

Este dato se genera de pilotos que no pudieron disputar un evento debido a problemas mecánicos, de salud, etc. pero que formaban parte de la competencia. Por ende, Gbriele Tarquini fue el piloto con más carreras no largadas con 40. Esto representa, acumuladamente, casi dos temporadas sin poder largar un gran premio.    
<img src="Formula1-Scrapy/Plots/p20.png" style="height: 50px; width:50px;"/>

**21. Los 10 pilotos con más carreras disputadas.**

<img src="Formula1-Scrapy/Plots/p21.png" style="height: 50px; width:50px;"/>
