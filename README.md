**ANÁLISIS DE DATOS**

**Autor: Edison Sanango**

En este repositorio hay dos carpetas **Fifa19** y **Formula1-Scrapy**. Cada carpeta tiene un proyecto, En **Fifa19** se realiza un proceso de principio a fin en el análisis de los datos del dataset Fifa19 de Kaggle incluyendo su visualización el cual se puede observar dentro del archivo README.md en su respectiva carpeta. En cambio, **Formula1-Scrapy** presenta un proceso similar con la diferencia de que los datos fueron recolectados en su totalidad mediante Web Scraping.
